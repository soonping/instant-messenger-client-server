CPSC 441 - Group 24 - Tutorial 02

Instant messenger system

source files:
-Server.java
-Client.java
-User.java

How to run Server:

-make sure to compile server.java
-run on command line:  java Server
-no arguments required, server will listen on port 5555
-Server uses the User class so User and Server must be in the same directory

How to run Client:

-make sure to compile client.java
-run on command line: java Client <server ip> <server port>
-two arguments required: server's ip address and the port it is running on
-Client is independent of Server and User